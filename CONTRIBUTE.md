Hagrid is owned by the [p≡p foundation] and licensed under the
terms of the AGPLv3+.

  [p≡p foundation]: https://pep.foundation/

To finance its mission, privacy by default, the [p≡p foundation]
allows third parties (currently only [p≡p security]) to relicense its
software.  Consistent with the rules of a foundation, the money
collected by the foundation in this manner is fully reinvested in the
foundation's mission, which includes further development of Hagrid.

  [p≡p security]: https://www.pep.security/

To do this, the [p≡p foundation] needs permission from all
contributors to relicense their changes.  In return, the
[p≡p foundation] guarantees that *all* releases of Hagrid (and
any other software it owns) will also be released under a GNU-approved
license.  That is, even if Foo Corp is granted a license to use
Hagrid in a proprietary product, the exact code that Foo Corp
uses will also be licensed under a GNU-approved license.

If you want to contribute to Hagrid, and you agree to the above,
please sign the [p≡p foundation]'s [CLA].  This is an electronic
assignment; no paper work is required.  You'll need to provide a valid
email address.  After clicking on a link to verify your email address,
you'll receive a second email, which contains the contract between you
and the [p≡p foundation].  Be sure to keep it for future reference.
The maintainers of Hagrid will also receive a notification.  At
that point, we can merge patches from you into Hagrid.

  [CLA]: https://contribution.pep.foundation/contribute/

Please direct questions regarding the CLA to [contribution@pep.foundation].

  [contribution@pep.foundation]: mailto:contribution@pep.foundation
